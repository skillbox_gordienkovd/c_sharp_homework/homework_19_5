﻿using System.Data.Entity;
using homework_19_5.Model;

namespace homework_19_5.Repository
{
    public class FaunaDbContext : DbContext
    {
        public FaunaDbContext() : base("fauna")
        {
            Database.SetInitializer(new AnimalTypeTableInitializer());
        }

        public DbSet<Animal> Animals { get; set; }
        public DbSet<AnimalType> AnimalTypes { get; set; }
    }
}