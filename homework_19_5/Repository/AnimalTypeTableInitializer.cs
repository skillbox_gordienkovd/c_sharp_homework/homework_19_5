﻿using System.Collections.Generic;
using System.Data.Entity;
using homework_19_5.Model;

namespace homework_19_5.Repository
{
    public class AnimalTypeTableInitializer : DropCreateDatabaseAlways<FaunaDbContext>
    {
        protected override void Seed(FaunaDbContext context)
        {
            IList<AnimalType> animalTypes = new List<AnimalType>();
            animalTypes.Add(new AnimalType {Type = "Bird"});
            animalTypes.Add(new AnimalType {Type = "Mammal"});
            animalTypes.Add(new AnimalType {Type = "Amphibian"});
            animalTypes.Add(new AnimalType {Type = "Unknown"});

            context.AnimalTypes.AddRange(animalTypes);

            base.Seed(context);
        }
    }
}