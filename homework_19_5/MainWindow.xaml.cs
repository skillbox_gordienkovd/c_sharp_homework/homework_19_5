﻿using System.Data.Entity;
using System.Windows;
using System.Windows.Controls;
using homework_19_5.Model;
using homework_19_5.Model.Factory;
using homework_19_5.Repository;
using homework_19_5.View;

namespace homework_19_5
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private readonly FaunaDbContext _faunaDbContext = new FaunaDbContext();

        public MainWindow()
        {
            InitializeComponent();
            
            _faunaDbContext.Animals.Load();

            AnimalsDataGrid.DataContext = _faunaDbContext.Animals.Local.ToBindingList();
        }
        
        private void AnimalsDataGridCellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            _faunaDbContext.SaveChanges();
        }
        
        private void MenuItemAddAnimalClick(object sender, RoutedEventArgs e)
        {
            var animal = new Animal();
            var animalAddWindow = new AnimalAddWindow(animal);
            
            animalAddWindow.ShowDialog();
            
            if (animalAddWindow.DialogResult == null || !animalAddWindow.DialogResult.Value) return;
            
            _faunaDbContext.Animals.Add(AnimalCreator.Create(animal));
            _faunaDbContext.SaveChanges();
        }
        
        private void MenuItemDeleteAnimalClick(object sender, RoutedEventArgs e)
        {
            var animal = (Animal)AnimalsDataGrid.SelectedItem;

            _faunaDbContext.Animals.Remove(animal);
            _faunaDbContext.SaveChanges();
        }

        private void Identification(object sender, RoutedEventArgs e)
        {
            var animal = (Animal)AnimalsDataGrid.SelectedItem;
            MessageBox.Show(animal.Identification());
        }
    }
}