﻿namespace homework_19_5.Model
{
    public class AnimalType
    {
        public long Id { get; set; }
        public string Type { get; set; }
    }
}