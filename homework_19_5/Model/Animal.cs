﻿using homework_19_5.Model.Factory;

namespace homework_19_5.Model
{
    public class Animal : IAnimal
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public long AnimalTypeId { get; set; }
        public AnimalType AnimalType { get; set; }

        public virtual string Identification()
        {
            return "Я неведомая зверюшка";
        }
    }
}