﻿using System;
using System.Linq;
using System.Windows;
using homework_19_5.Repository;

namespace homework_19_5.Model.Factory
{
    public class AnimalCreator
    {
        private const string FactoryPath = "homework_19_5.Model.Factory.";

        public static Animal Create(Animal animal)
        {
            try
            {
                var faunaContext = new FaunaDbContext();
                var animalType = faunaContext.AnimalTypes.Find(animal.AnimalTypeId);
                var classType = Type.GetType(FactoryPath + animalType?.Type);

                if (classType == null) return animal;

                var certainAnimal = (Animal) Activator.CreateInstance(classType);
                certainAnimal.Id = animal.Id;
                certainAnimal.Name = animal.Name;
                certainAnimal.AnimalTypeId = animal.AnimalTypeId;
                certainAnimal.AnimalType = animal.AnimalType;

                return certainAnimal;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return animal;
            }
        }
    }
}