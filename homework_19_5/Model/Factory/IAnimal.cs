﻿namespace homework_19_5.Model.Factory
{
    public interface IAnimal
    {
        string Identification();
    }
}