﻿using System.Data.Entity;
using System.Linq;
using homework_19_5.Model;
using homework_19_5.Repository;

namespace homework_19_5.View
{
    public partial class AnimalAddWindow
    {
        private readonly FaunaDbContext _faunaDbContext = new FaunaDbContext();
        private AnimalAddWindow()
        {
            InitializeComponent();
            
            _faunaDbContext.AnimalTypes.Load();

            var animalTypeItems = _faunaDbContext.AnimalTypes.ToList();
            AnimalTypes.ItemsSource = animalTypeItems;
            AnimalTypes.DisplayMemberPath = "Type";
        }

        public AnimalAddWindow(Animal animal) : this()
        {
            Cancel.Click += delegate
            {
                DialogResult = false;
            };

            Add.Click += delegate
            {
                var animalType = (AnimalType)AnimalTypes.SelectionBoxItem;
                animal.Name = AnimalName.Text;
                animal.AnimalTypeId = animalType.Id;

                DialogResult = true;
            };
        }
    }
}